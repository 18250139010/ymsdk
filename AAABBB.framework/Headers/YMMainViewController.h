//
//  YMMainViewController.h
//  有美生活
//
//  Created by mac on 2021/6/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YMMainViewController : UIViewController
@property (copy, nonatomic) NSString *hierarchyNum;
@property (copy, nonatomic) NSString *comeType;
@property (copy, nonatomic) NSString *navTitle;

@end

NS_ASSUME_NONNULL_END
