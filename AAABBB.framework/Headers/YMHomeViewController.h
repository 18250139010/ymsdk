//
//  YMHomeViewController.h
//  有美生活
//
//  Created by mac on 2021/6/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YMHomeViewController : UIViewController

@property (nonatomic ,strong)UINavigationController *nva;
@property (copy, nonatomic) NSString *hierarchyNum;
@property (copy, nonatomic) NSString *templateId;
@property (copy, nonatomic) NSString *comeType;

@property (copy, nonatomic) NSString *linkType;

@property (copy, nonatomic) NSString *navTitle;


@end

NS_ASSUME_NONNULL_END
