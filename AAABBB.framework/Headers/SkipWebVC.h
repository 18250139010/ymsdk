//
//  SkipWebVC.h
//  DaiMengJia
//
//  Created by 呆萌价 on 2018/6/4.
//  Copyright © 2018年 呆萌价. All rights reserved.
//

//#import "BaseViewController.h"

@interface SkipWebVC : UIViewController

@property (copy, nonatomic) NSString *urlStr;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *popType;
@property (copy, nonatomic) NSString *comeType;
@property (copy, nonatomic) NSString *navTitle;
@end



